# Extract certain category id from COCO dataset and convert to labelme format
## Usage
```bash
python coco_cat_to_labelme.py [cat_id] [cat_name] [ann_file] [img_dir] [output_dir] [--type]
```

参数说明：  
- cat_id: 目标在 COCO 数据集的 id
- cat_name: 类别名称
- ann_file: COCO 格式的 json 标注文件
- img_dir: 图片数据文件夹
- output_dir: 输出文件夹
- `--type`: 标注格式，`box` 或 `seg`，默认为 `box`

## Result
- 输出文件夹中会包含名称对应的 json 和 image
- example
```bash
00001111.json 00001111.jpg
00002222.json 00002222.jpg
00003333.json 00003333.jpg
......
```