#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: willc
"""
from argparse import ArgumentParser
from utils import *
import os

if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("cat_id", help="coco category ID")
    parser.add_argument("cat_name", help="coco category name")
    parser.add_argument("ann_file", help="coco annotation json file")
    parser.add_argument("img_dir", help="coco image directory")
    parser.add_argument("output_dir", help="output directory")
    parser.add_argument("--type", help="annotation type, box or seg", default='box')

    args = parser.parse_args()

    os.makedirs(args.output_dir, exist_ok=True)
    print("Creating info dict")
    info = group_coco_info_by_imgId(args.ann_file)

    print("Creating annotation dict")
    filter_anno = filter_annos(args.ann_file, int(args.cat_id))
    anno = group_coco_annos_by_imgId(filter_anno)

    print("Converting to Labelme format")
    if args.type == "box":
        convert2labelme(args.cat_name, info, anno, args.img_dir, args.output_dir)
    else:
        convert2labelme_polygons(args.cat_name, info, anno, args.img_dir, args.output_dir)

    print("Copying images to destination")
    copy_images(info, anno, args.img_dir, args.output_dir)
    print("Finished!")