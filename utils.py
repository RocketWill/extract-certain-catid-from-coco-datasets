#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: willc
"""

import json
import os
import base64
from collections import defaultdict
import numpy as np
import pycocotools._mask as maskutil
from skimage import measure

no_copy = []

def group_coco_info_by_imgId(ann_file):
    info = defaultdict(None)
    with open(ann_file , 'r') as reader:
        jf = json.loads(reader.read())
        images = jf['images']
        try:
            for img in images:
                file_name = img['file_name']
                height = img['height']
                width = img['width']
                id = img['id']

                info[id] = {
                    'file_name': file_name,
                    'height': height,
                    'width': width
                }
        except Exception as e:
            print(e)
            raise
    return info

def filter_annos(ann_file, catId):
    filter_res = []
    with open(ann_file, 'r') as reader:
        jf = json.loads(reader.read())
        annos = jf['annotations']
        try:
            for anno in annos:
                cat_id = anno['category_id']
                iscrowd = anno['iscrowd']
                if int(iscrowd) == 1: continue
                if cat_id == catId:
                    filter_res.append(anno)
        except Exception as e:
            print(e)
            raise
    return filter_res

def group_coco_annos_by_imgId(filter_ann):
    print(len(filter_ann))
    anno = defaultdict(list)
    try:
        for det in filter_ann:
            image_id = det['image_id']
            anno[image_id].append(det)
    except Exception as e:
        print(e)
        raise
    return anno

def convert2labelme(cat_name, info_dict, anno_dict, image_dir, output_dir):

    def create_labelme_obj(h, w):
        return {
            "version": "4.2.10",
            "flags": {},
            "shapes": [],
            "imagePath": "",
            "imageData": "",
            "imageHeight": h,
            "imageWidth": w
        }

    def create_shape_obj(label, points):
        return {
            "label": label,
            "points": points,
            "group_id": None,
            "shape_type": "rectangle",
            "flags": {}
        }

    for id, anno in anno_dict.items():
        info = info_dict[id]
        file_name = info['file_name']
        height = info['height']
        width = info['width']

        labelme_obj = create_labelme_obj(height, width)
        for det in anno:
            x, y, w, h = det['bbox']
            shape = create_shape_obj(cat_name, [[x, y], [x + w, y + h]])
            labelme_obj['shapes'].append(shape)

        labelme_obj['imagePath'] = file_name
        img_path = os.path.join(image_dir, file_name)

        with open(img_path, "rb") as imageFile:
            img_data = base64.b64encode(imageFile.read())
            labelme_obj['imageData'] = img_data.decode("utf-8")

        output_file_name = file_name.split('.')[0]
        with open(os.path.join(output_dir, output_file_name + '.json'), 'w') as outfile:
            json.dump(labelme_obj, outfile)

def convert2labelme_polygons(cat_name, info_dict, anno_dict, image_dir, output_dir):
    global no_copy

    def create_labelme_obj(h, w):
        return {
            "version": "4.2.10",
            "flags": {},
            "shapes": [],
            "imagePath": "",
            "imageData": "",
            "imageHeight": h,
            "imageWidth": w
        }

    def create_shape_obj(label, points):
        return {
            "label": label,
            "points": points,
            "group_id": None,
            "shape_type": "polygon",
            "flags": {}
        }

    def check(anno_list):
        flag = True
        for det in anno_list:
            seg = det['segmentation']
            if len(seg) > 1:
                flag = False
                break
        return flag


    for id, anno in anno_dict.items():
        if not check(anno):
            no_copy.append(id)
            continue

        info = info_dict[id]
        file_name = info['file_name']
        height = info['height']
        width = info['width']

        labelme_obj = create_labelme_obj(height, width)
        for det in anno:

            seg = det['segmentation']
            seg = cocopoly2labelmepoly(seg)
            shape = create_shape_obj(cat_name, seg)
            labelme_obj['shapes'].append(shape)

        labelme_obj['imagePath'] = file_name
        img_path = os.path.join(image_dir, file_name)
        labelme_obj['imageData'] = None

        # with open(img_path, "rb") as imageFile:
        #     img_data = base64.b64encode(imageFile.read())
        #     labelme_obj['imageData'] = img_data.decode("utf-8")

        output_file_name = file_name.split('.')[0]
        with open(os.path.join(output_dir, output_file_name + '.json'), 'w') as outfile:
            json.dump(labelme_obj, outfile)

def copy_images(info_mapper, anno_mapper, image_dir, output_dir):
    global no_copy
    for id, anno in anno_mapper.items():
        if id in no_copy:
            continue
        img_path = os.path.join(image_dir, info_mapper[id]['file_name'])
        cmd = "cp {} {}".format(img_path, output_dir)
        os.system(cmd)


def cocopoly2labelmepoly(coco_poly):
    def to_point_list(list):
        res = []
        temp = []
        for i, point in enumerate(list):
            if i % 2 == 0:
                temp.append(point)
            else:
                temp.append(point)
                res.append(temp)
                temp = []
        return res

    def get_max_polygon(polygons_list):
        max_area = 0
        max_polygon = polygons_list[0]
        for polygon in polygons_list:
            all_x = [x[0] for x in polygon]
            all_y = [y[1] for y in polygon]
            max_x = max(all_x)
            min_x = min(all_x)
            max_y = max(all_y)
            min_y = min(all_y)
            area = (max_x-min_x)*(max_y-min_y)
            if area > max_area:
                max_area = area
                max_polygon = polygon
        return max_polygon


    if len(coco_poly) > 1:
        all_polygons = []
        for poly in coco_poly:
            poly = to_point_list(poly)
            all_polygons.append(poly)
        return get_max_polygon(all_polygons)

    else:
        return to_point_list(coco_poly[0])

